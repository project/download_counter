DESCRIPTION
---------------

A module which helps display a counter of Firefox downloads as syndicated from http://spreadfirefox.com. This module
can easily be used to display the body of any RSS feed item. Just change the URL mentioned below.

INSTALL
-------------------

- Install this module as usual
- If not enabled already, activate aggregator.module. This module comes with Drupal.
- Add a feed whose URL is http://feeds.spreadfirefox.com/downloads/firefox
- Now decide where you want the counter to display. You can put it in your theme, or in a custom block, or in a specific node, etc.
All you need to do (for a block) is select the 'PHP code' input format and enter <?php print download_counter_get(); ?>
